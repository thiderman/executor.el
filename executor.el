;;; executor.el --- Tools for running executable targets in compile buffers -*- lexical-binding: t -*-

;; Copyright (C) 2019 Lowe Thiderman

;; Author: Lowe Thiderman <thiderman@protonmail.com>
;; URL: https://gitlab.com/thiderman/executor.el
;; Package-Version: 20190316
;; Version: 0.5.0
;; Package-Requires: ((emacs "24.3") (projectile "1.0.0") (dash "2.11.0") (f "0.11.0") (s "1.10.0"))
;; Keywords: processes

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; A set of tools aimed at working with executor entrypoints, i.e. Makefiles,
;; docker-compose files, npm commands, et.c.
;;
;; To facilitate this, this package introduces the concept of _executor files_.
;; An _executor file_ is any file that defines endpoints accessible by command
;; line (i.e. a Makefile) and a _target_ is any argument passed when invoking.
;; So, in the invocation of `make test`, `make` is the _executor_ and `test` is
;; the target. This package is all about giving convenient access to determine
;; those and executing them in `compilation' buffers.
;;
;; The main entrypoint for the package is `executor-execute`. If executed in a supported
;; file, (i.e. a `Makefile`) it will prompt the user for which target to
;; execute. If in any other buffer, the user is prompted which supported
;; executor file they want to invoke from. Once an _executor_ and a _target_ has
;; been chosen, a dedicated `compilation' buffer is spawned, with its name
;; being set to `*<project>:<target>*`, i.e. `*bitcoin-miner-malware:deploy*`.
;;
;; When in a dedicated executor buffer, `executor-execute` can be used again to select a
;; different _target_. Furthermore, `executor-visit-file` can be used to visit the
;; _executor_ file that spawned the command. And, once we've spun up a few
;; buffers, we can use `executor-select-buffer` to visit a specific one.
;;
;; Lastly, when visiting a _executor_ file, `executor-mode` can be enabled. This boldly
;; binds `C-c C-c` to `executor-execute` and sets up some caching logic that speeds up
;; evaluation of available targets. Setting up the minor mode is a bit
;; different, since the different _executors_ come in various file formats that
;; don't share any major mode in common. See the _Installation_ section of this
;; document.
;;
;;; Specific executor functionality
;; - The `Makefile` executor has calculation of variables, i.e. $(BINARY)
;;   will show up as what it evaluates to when invoking the Makefile.
;; - The `docker-compose` will first prompt for a command to run (`up`, `ps`,
;;   et.c.) and then optionally for which service you want to target. No
;;   argument is needed for `ps`, but for `up` and other service oriented
;;   commands it makes sense to provide one.
;; - The `magefile` executor will retain any docstrings that the targets have,
;;   and they will be fontified as comments in the selection.
;;
;;; Supported executors:
;; - Makefiles
;; - Magefiles (https://magefile.org)
;; - docker-compose
;;
;;; Installation
;; Using `use-package` is the easiest way;
;;
;;      (use-package executor
;;        :bind (:map compilation-mode-map
;;                    ("e" . executor-execute)
;;                    ("f" . executor-visit-file)
;;                    ("b" . executor-select-buffer))
;;        :bind-keymap (("C-x C-m" . executor-global-map))
;;        :hook
;;        (prog-mode . executor-maybe-enable-mode)
;;        (text-mode . executor-maybe-enable-mode))
;;
;; Perhaps obviously, the use of the keymaps and hooks are optional but
;; recommended.
;;
;; The hooks hook in to the highest level ones as to not miss out by using major
;; mode specific ones. The calculations that `executor-maybe-enable-mode` does are very
;; lightweight and should not cause any performance issues when opening buffers.
;;
;;; Extending
;; This is the kind of project that benefits immensely from collaboration. The
;; three first _executors_ (Make, mage, docker-compose) are the ones that the
;; original author uses in their daily workflow. However, the core code of the
;; project has been written as to be as extendable as possible. Writing new
;; executors can be done in less than 100 lines of elisp.
;;
;; If you want to add a new executor, base it off of `executor-mage.el`, since it's both
;; the least complicated (~70 lines at the time of writing) and the most
;; featureful (it uses both caching and fontification). Oh, and if you run in to
;; trouble, open an issue in the repository and the maintainer will help out.
;;
;; Ideas for executors;
;; - `npm run` targets
;; - Django `manage.py` (and other similar web frameworks)
;; - `Rakefiles` for Ruby projects
;; - Rust `cargo` commands
;; - et.c.
;;
;; Basically, any command line tool that has a large set of commands (especially
;; when the user can define new ones specific to their project) can have an
;; executor implemented.
;;
;;; Code:

(require 'compile)
(require 'dash)
(require 'f)
(require 'make-mode)
(require 's)
(require 'projectile)

(defvar executor-global-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "b") #'executor-select-buffer)
    (define-key map (kbd "e") #'executor-execute)
    (define-key map (kbd "f") #'executor-visit-file)
    (define-key map (kbd "p") #'executor-project-execute)
    (define-key map (kbd "C-m") #'executor-execute)
    map)
  "Global keymap for `executor-mode'.")

(defvar executor-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c C-c") #'executor-execute)
    map)
  "Keymap for `executor-mode'.")

(define-minor-mode executor-mode
  "Turn `executor' mode on if ARG is positive, off otherwise.

\\{executor-mode-map}"
  :global nil
  :lighter " exec"
  :keymap executor-mode-map)


(setq executor-cache (make-hash-table :test 'equal))

(defgroup executor nil
  "Conveniently running executor entrypoints."
  :group 'convenience
  :prefix "executor-")

(defcustom executor-ignore "vendor/"
  "Regexp of paths that should be filtered when looking for target files."
  :type 'string)

(require 'executor-core)

;;* Base helper functions
(defun executor--select-file ()
  "Select an applicable executor file in the current project."

  ;; TODO(thiderman): There seem to be a lot of people that don't favor
  ;; `projectile' and wouldn't install a package that depends on it. Perhaps
  ;; make it optional and do some other kind of file and root detection?

  ;; To be able to find other files, we need either a file attached to the
  ;; current buffer, or to be in a project.
  (unless (or (projectile-project-p)
              (buffer-file-name))
    (user-error "No project or buffer; cannot determine how to find files"))

  (let* ((files (-mapcat 'executor-get-files executor-executors))
         (file (concat
                (projectile-project-root)
                ;; If there is just the one file, return that one immediately
                (if (= (length files) 1)
                    (first files)
                  ;; If there are more, do a completing read, and use the
                  ;; closest executor in the project as an initial input, if
                  ;; possible.
                  (completing-read "executor files: " files nil t
                                   ;; TODO(thiderman): Re-implement
                                   ;; (executor-get-files-initial-input exc files)
                                   )))))
    (when (null file)
      (user-error "No executor file found"))

    ;; TODO(thiderman): Perhaps improve this by also returning the executor?
    file))

(defun executor-maybe-enable-mode ()
  "Hook that enables the minor mode for the current buffer, if applicable."
  (interactive)
  ;; If we successfully return an executor, it means that we have something that
  ;; can handle the current file, and as such we should enable the mode.
  (when (executor--get-executor-for-file (buffer-file-name))
    (executor-mode 1)))

(defun executor-after-save ()
  "If the minor mode is enabled, run the executor post actions."
  (interactive)
  (when executor-mode
    (let ((filename (buffer-file-name)))
      (executor-after-save-action (executor--get-executor-for-file filename) filename))))

;;* Entry points
;;;###autoload
(defun executor-execute (&optional filename target)
  "Run an executor target.

If we are visiting a file that we can execute, targets from that file will be
presented. If we're not, we prompt for an executor file to execute from.

The target will be executed in a dedicated buffer named
\"*<project>:<target>*\"."
  (interactive
   (list (and buffer-file-name
              (file-truename buffer-file-name))))

  (let* ((exc (and filename (executor--get-executor-for-file filename))))
    ;; So, above we run -get-executor-for-file, covering the case where we have
    ;; interactively called this function and might want to operate on the file
    ;; we are currently visiting.
    (when (not exc)
      ;; However, it's possible that the file itself is not a file that any of
      ;; our executors can handle. If that's the case, we have two options;
      ;; - If the buffer is a dedicated buffer, choose the executor that spawned it.
      ;; - If not, interactively choose one of the applicable files in the project.
      (setq filename (or executor--filename (executor--select-file)))

      ;; If it didn't work, we'll signal a fairly generic error.
      (when (not filename)
        (user-error "No executor file found"))

      ;; And if it did work, that means that we can find the applicable executor
      ;; again so that we have it.
      (setq exc (executor--get-executor-for-file filename)))

    (executor-execute-target exc filename target t)))

;;;###autoload
(defun executor-project-execute ()
  "Like `executor-execute', but always select which file to execute from the project."
  (interactive)

  ;; TODO(thiderman): This basically exists because we want to be able to have
  ;; the autodetection in (interactive) to default to the current file that you
  ;; are in. Preferrably, we would just use the universal argument, but I
  ;; haven't found a way to combine both the universal argument and also
  ;; optional interactive arguments.
  (executor-execute))

;;;###autoload
(defun executor-visit-file (&optional filename)
  "Visit an executor file.

If we are in a dedicated executor buffer, visit the file that spawned it.

If not, prompt for one in the project. If there is just one, go to it immediately"
  (interactive)

  (find-file (or filename executor--filename (executor--select-file))))

;;;###autoload
(defun executor-select-buffer (&optional name)
  "Choose a dedicated executor buffer to visit."
  (interactive)

  ;; Get a list of all buffers that have a non-nil `executor--filename'.
  (let ((buffers (--map (buffer-name it)
                        (--filter (with-current-buffer it executor--filename)
                                  (buffer-list)))))
    (if (zerop (length buffers))
        (user-error "There are no dedicated executor buffers open right now")
      (switch-to-buffer (completing-read "executor buffers: " buffers nil t)))))

(provide 'executor)
