;;* Executor core
(defvar executor-executors nil
  "Executors that are enabled.")

;; The executor filename that spawned the current buffer
(setq-default executor--filename nil)

;;* Main class definition
(defclass executor ()
  ((name :initarg :name
         :type string
         :documentation
         "The name of the executor class.")))

(cl-defmethod executor-name ((exc executor))
  "Get the name of the current executor."

  (oref exc name))

;;** Execute
(cl-defmethod executor-execute-target ((exc executor)
                                       filename &optional target dedicated)
  "Execute an executor target from FILENAME.

FILENAME defaults to current buffer. Will prompt for TARGET if
none is provided.

The execution happens with `default-directory' set to the parent
of FILENAME.

If the optional DEDICATED argument is set, the compilation buffer is immediately
renamed to \"*<project>:<target>*\""
  (let* ((target (or target (executor-select-target exc filename)))
         (default-directory (f-dirname filename))
         (buffer-name (executor-buffer-name exc target)))
    ;; Check if we're already running a dedicated buffer for the thing we are
    ;; about to execute. If so, switch to that buffer rather than starting the
    ;; process again.
    (if (get-buffer buffer-name)
        ;; TODO(thiderman): Perhaps someone wants to be able to have several
        ;; buffers? Add a configuration override?
        (progn
          (switch-to-buffer buffer-name)
          (message "Switched to existing buffer %s" buffer-name))
      (compile (executor-compile-command exc filename target))
      (when dedicated
        (with-current-buffer (get-buffer "*compilation*")
          (rename-buffer buffer-name)
          (set (make-local-variable 'executor--filename) filename))))))

(cl-defmethod executor-buffer-name ((exc executor) target)
  "Return what to call a buffer generated for this executor.

Used so that specific executors can override and add more details."
  (format "*%s:%s*" (projectile-project-name) target))



;;** Executor file selection
(cl-defgeneric executor-get-files ((exc executor))
  "Get all the executor files for a project.")

(cl-defmethod executor-get-files-initial-input ((exc executor)
                                                files)
  "Optionally set the initial input for `executor-get-files' if there
are multiple targets.

This is to help the user find an executor file as close as
possible to the current buffer."
  nil)

;;** Target selection
(cl-defmethod executor-select-target ((exc executor)
                                      filename)
  "Execute a target from FILENAME."
  (let ((targets (executor-get-targets exc filename)))
    (executor-clean-target
     exc
     (completing-read
      "target: "
      (mapcar (lambda (x)
                (executor-decorate-target exc x))
              targets)))))

(cl-defgeneric executor-get-targets ((exc executor) filename)
  "Return a list of all the possible execution targets in
  FILENAME.")

(cl-defmethod executor-clean-target ((exc executor) target)
  "Optionally prepares the target after it's selected.

Use whenever the target also includes a comment string."
  target)

(cl-defmethod executor-decorate-target ((exc executor)
                                        target)
  "Optional decorating function that can add fontification to a
target, if a subclass so desires."
  target)

(cl-defgeneric executor-compile-command ((exc executor)
                                         filename target)
  "Return the string to be passed to `compile' based on a
  FILENAME and a TARGET.")

;;** Cache functions
(cl-defmethod executor-get-cache ((exc executor) filename)
  "Get the results of targets from FILENAME."
  (gethash (file-truename filename) executor-cache))

(cl-defmethod executor-store-cache ((exc executor) filename targets)
  "Store targets for FILENAME.

Returns the targets list."
  (puthash (file-truename filename) targets executor-cache)
  targets)

(cl-defmethod executor-clear-cache ((exc executor) filename)
  "Clear the cache for FILENAME."
  (puthash (file-truename filename) nil executor-cache))

;;** Helper functions
(cl-defgeneric executor-handle-file-p ((exc executor) filename)
  "Return `t' if the executor in question can parse and execute FILENAME.")

(defun executor--get-executor-for-file (filename)
  "Return the executor class that can operate on FILENAME"
  (when filename
    (-first (lambda (exc) (executor-handle-file-p exc filename))
            executor-executors)))
;;** Hooks
(cl-defmethod executor-after-save-action ((exc executor) filename)
  "Hook to run after saving an executor file.

Can be used to clear the cache or similar post-operations.")

;;* Initialization of executors
(require 'executor-make)
(require 'executor-mage)
(require 'executor-compose)
(require 'executor-bazel)
(require 'executor-task)

(add-to-list 'executor-executors (executor-make :name "make"))
(add-to-list 'executor-executors (executor-mage :name "mage"))
(add-to-list 'executor-executors (executor-compose :name "compose"))
(add-to-list 'executor-executors (executor-bazel :name "bazel"))
(add-to-list 'executor-executors (executor-task :name "task"))

(provide 'executor-core)
