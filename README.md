## executor.el
*Tools for running executable targets in compile buffers*

---
[![License GPLv3](https://img.shields.io/badge/license-GPL_v3-green.svg)](http://www.gnu.org/licenses/gpl-3.0.html)

A set of tools aimed at working with executor entrypoints, i.e. Makefiles,
docker-compose files, npm commands, et.c.

To facilitate this, this package introduces the concept of _executor files_.
An _executor file_ is any file that defines endpoints accessible by command
line (i.e. a Makefile) and a _target_ is any argument passed when invoking.
So, in the invocation of `make test`, `make` is the _executor_ and `test` is
the target. This package is all about giving convenient access to determine
those and executing them in `compilation` buffers.

The main entrypoint for the package is `executor-execute`. If executed in a supported
file, (i.e. a `Makefile`) it will prompt the user for which target to
execute. If in any other buffer, the user is prompted which supported
executor file they want to invoke from. Once an _executor_ and a _target_ has
been chosen, a dedicated `compilation` buffer is spawned, with its name
being set to `*<project>:<target>*`, i.e. `*bitcoin-miner-malware:deploy*`.

When in a dedicated executor buffer, `executor-execute` can be used again to select a
different _target_. Furthermore, `executor-visit-file` can be used to visit the
_executor_ file that spawned the command. And, once we've spun up a few
buffers, we can use `executor-select-buffer` to visit a specific one.

Lastly, when visiting a _executor_ file, `executor-mode` can be enabled. This boldly
binds `C-c C-c` to `executor-execute` and sets up some caching logic that speeds up
evaluation of available targets. Setting up the minor mode is a bit
different, since the different _executors_ come in various file formats that
don't share any major mode in common. See the _Installation_ section of this
document.

### Specific executor functionality

- The `Makefile` executor has calculation of variables, i.e. $(BINARY)
  will show up as what it evaluates to when invoking the Makefile.
- The `docker-compose` will first prompt for a command to run (`up`, `ps`,
  et.c.) and then optionally for which service you want to target. No
  argument is needed for `ps`, but for `up` and other service oriented
  commands it makes sense to provide one.
- The `magefile` executor will retain any docstrings that the targets have,
  and they will be fontified as comments in the selection.

### Supported executors

- Makefiles
- Magefiles (https://magefile.org)
- docker-compose

### Installation

Using `use-package` is the easiest way;

     (use-package executor
       :bind (:map compilation-mode-map
                   ("e" . executor-execute)
                   ("f" . executor-visit-file)
                   ("b" . executor-select-buffer))
       :bind-keymap (("C-x C-m" . executor-global-map))
       :hook
       (prog-mode . executor-maybe-enable-mode)
       (text-mode . executor-maybe-enable-mode))

Perhaps obviously, the use of the keymaps and hooks are optional but
recommended.

The hooks hook in to the highest level ones as to not miss out by using major
mode specific ones. The calculations that `executor-maybe-enable-mode` does are very
lightweight and should not cause any performance issues when opening buffers.

### Extending

This is the kind of project that benefits immensely from collaboration. The
three first _executors_ (Make, mage, docker-compose) are the ones that the
original author uses in their daily workflow. However, the core code of the
project has been written as to be as extendable as possible. Writing new
executors can be done in less than 100 lines of elisp.

If you want to add a new executor, base it off of `executor-mage.el`, since it's both
the least complicated (~70 lines at the time of writing) and the most
featureful (it uses both caching and fontification). Oh, and if you run in to
trouble, open an issue in the repository and the maintainer will help out.

Ideas for executors;
- `npm run` targets
- Django `manage.py` (and other similar web frameworks)
- `Rakefiles` for Ruby projects
- Rust `cargo` commands
- et.c.

Basically, any command line tool that has a large set of commands (especially
when the user can define new ones specific to their project) can have an
executor implemented.

### Function Documentation


#### `(executor-maybe-enable-mode)`

Hook that enables the minor mode for the current buffer, if applicable.

#### `(executor-after-save)`

If the minor mode is enabled, run the executor post actions.

#### `(executor-execute &optional FILENAME TARGET)`

Run an executor target.

If we are visiting a file that we can execute, targets from that file will be
presented. If we’re not, we prompt for an executor file to execute from.

The target will be executed in a dedicated buffer named
"*<project>:<target>*".

#### `(executor-project-execute)`

Like ‘executor-execute’, but always select which file to execute from the project.

#### `(executor-visit-file &optional FILENAME)`

Visit an executor file.

If we are in a dedicated executor buffer, visit the file that spawned it.

If not, prompt for one in the project. If there is just one, go to it immediately

#### `(executor-select-buffer &optional NAME)`

Choose a dedicated executor buffer to visit.

-----
<div style="padding-top:15px;color: #d0d0d0;">
Markdown README file generated by
<a href="https://github.com/mgalgs/make-readme-markdown">make-readme-markdown.el</a>
</div>
