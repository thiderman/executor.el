;;* Docker Compose files
(defclass executor-compose (executor)
  ())

(defvar executor-compose-extra-token "*no argument*"
  "Special token used to mark usage of all services when selecting
  interactively")

(defvar executor-docker-compose-last-run
  ""
  "Last run service. Used for building buffer names.")

(defvar executor-docker-compose-commands
  (list
         "build"
         "build --no-cache"
         "logs --follow"
         "logs --follow --tail=100"
         "up"
         "up --build"
         "create"
         "down"
         "exec"
         "images"
         "logs"
         "ps"
         "pull"
         "push"
         "restart"
         "rm"
         "rm -f"
         "run"
         "start"
         "stop"
         "top"
         )
  "Commands that can be executed")

(defun executor-compose-select-service (filename)
  "Get a list of all the services in FILENAME"
  (let ((services (split-string
                   (shell-command-to-string
                    (format "docker-compose -f \"%s\" config --services"
                            filename)))))
    (add-to-list 'services executor-compose-extra-token)
    (setq executor-docker-compose-last-run
          (completing-read "service: " services))))

(cl-defmethod executor-get-targets ((exc executor-compose)
                                    filename)
  "Returns all executable commands"

  (let* ((default-directory (f-dirname filename)))
    executor-docker-compose-commands))

(cl-defmethod executor-get-files ((exc executor-compose))
  "Get all the composefiles in the current project"
  (-filter (lambda (f) (executor-handle-file-p exc f))
           (projectile-current-project-files)))

(cl-defmethod executor-handle-file-p ((exc executor-compose) filename)
  "Returns `t' if the executor in question can parse and execute FILENAME."
  (and (s-contains? "docker-compose" (s-downcase filename))
       (not (string-match executor-ignore filename))))

(cl-defmethod executor-compile-command ((exc executor-compose)
                                        filename target &optional service)
  "docker-compose is special in the sense that you need to select _two_ things;
which service to run and which command to execute."

  (let ((target (format "docker-compose -f \"%s\" %s" filename target))
        (service (or service (executor-compose-select-service filename))))

    ;; If a service is set, we need to add it to the command
    (when (not (string-equal service executor-compose-extra-token))
      (setq target (format "%s %s" target service)))
    target))

(cl-defmethod executor-buffer-name ((exc executor-compose) target)
  "Return what to call a buffer generated for this executor.

Used so that specific executors can override and add more details."
  (format "*%s:%s:%s*"
          (projectile-project-name)
          target
          executor-docker-compose-last-run))

(provide 'executor-compose)
