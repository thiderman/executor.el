.PHONY: test
test:
	echo "hehe"

README.md: make-readme-markdown.el executor.el
	emacs -Q --script $< <executor.el >$@ 2>/dev/null

make-readme-markdown.el:
	wget -q -O $@ https://raw.github.com/mgalgs/make-readme-markdown/master/make-readme-markdown.el

.INTERMEDIATE: make-readme-markdown.el
