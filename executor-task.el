;;* Taskfile support
(defclass executor-task (executor)
  ())

(cl-defmethod executor-get-targets ((exc executor-task)
                                    filename)
  "Return a list of all the targets of a Taskfile.

Receives FILENAME, and runs `task' in the directory of said
file."

  (let* ((default-directory (f-dirname filename)))
    (executor-store-cache
     exc
     filename
     (rest (s-split "\n" (shell-command-to-string "task -l") t)))))

(cl-defmethod executor-get-files ((exc executor-task))
  "Get all the taskfiles in the current project"
  (-filter (lambda (f) (executor-handle-file-p exc f))
           (projectile-current-project-files)))

(cl-defmethod executor-handle-file-p ((exc executor-task) filename)
  "Returns `t' if the executor in question can parse and execute FILENAME."
  (and (s-contains? "Taskfile.yml" filename)
       (not (string-match executor-ignore filename))))

(cl-defmethod executor-compile-command ((exc executor-task)
                                        filename target)
  "Builds the task command that can execute the TARGET."

  (format "task %s" target))

(cl-defmethod executor-decorate-target ((exc executor-task)
                                        target)
  "Splits the line and colorizes the namespace and the target"

  (let* ((words (s-split ":" (s-trim-left target)))
         (namespace (s-chop-prefix "* " (first words)))
         (split (s-split " " (second words)))
         (target (first split))
         (doc (s-trim-left (car (last words))))
         (decorated (concat
                     (propertize namespace 'face 'font-lock-keyword-face)
                     (when (not (s-equals? "" target))
                       (concat
                        (propertize ":" 'face 'font-lock-comment-delimiter-face)
                        (propertize target 'face 'font-lock-function-name-face)))))
         (spacing
          (s-repeat (- 20 (length decorated))
                    " ")))
    (concat
     decorated
     (when current-prefix-arg
       (concat spacing
               (propertize doc 'face 'font-lock-comment-delimiter-face))))))

(cl-defmethod executor-clean-target ((exc executor-task)
                                     target)
  "Remove the docstring and the fontification from the target."
  (substring-no-properties (first (s-split " " target))))

(cl-defmethod executor-after-save-action ((exc executor-task) filename)
  "Clear the cache so that any target modifications show up."
  (executor-clear-cache exc filename))

(provide 'executor-task)
