;;* Bazel files

(defclass executor-bazel (executor)
  ())

(cl-defmethod executor-get-targets ((exc executor-bazel)
                                    filename)
  ""
  (let* ((default-directory (f-dirname filename))
         (cache (executor-get-cache exc filename))
         (out (shell-command-to-string
               "bazel query 'attr(visibility, \"//visibility:public\", ...)'"))
         (targets (-filter
                   (lambda (s)
                     (s-starts-with? "//" s))
                   (s-split "\n" out t))))
    (if cache
        cache
      (executor-store-cache exc filename targets))))

(cl-defmethod executor-compile-command ((exc executor-bazel)
                                        filename target)
  "Builds the make command that can execute the TARGET."

  (format "bazel run %s" target))

(cl-defmethod executor-get-files ((exc executor-bazel))
  "Get all the makefiles in the current project"
  (-filter (lambda (f) (executor-handle-file-p exc f))
           (projectile-current-project-files)))

(cl-defmethod executor-handle-file-p ((exc executor-bazel) filename)
  "Returns `t' if the executor in question can parse and execute FILENAME."
  (and (s-ends-with? "WORKSPACE" filename)
       (not (string-match executor-ignore filename))))

(provide 'executor-bazel)
