;;* Makefile

(defvar executor-special-make-target "emacs--makefile--list")

;; Based on http://stackoverflow.com/a/26339924/983746
(defvar executor--make-list-target-code
  (format
   ".PHONY: %s\n%s:\n	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ \"^[#.]\") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'\n"
   executor-special-make-target executor-special-make-target)
  "Target used to list all other Makefile targets.")

(defclass executor-make (executor)
  ())

(cl-defmethod executor-get-targets ((exc executor-make)
                                    filename)
  "Return a list of all the targets of a Makefile.

To list them in a computed manner, a new special target is added,
the buffer is written to a temporary Makefile which is executed
with the special target."
  (let* ((file (make-temp-file "makefile"))
         (makefile-contents
          (concat
           (with-temp-buffer
             (insert-file-contents filename)
             (buffer-string))
           "\n\n"
           executor--make-list-target-code)))

    (f-write-text makefile-contents 'utf-8 file)

    (let ((out (shell-command-to-string
                (format "make -f %s %s"
                        (shell-quote-argument file)
                        executor-special-make-target))))
      (delete-file file)
      (s-split "\n" out t))))

(cl-defmethod executor-compile-command ((exc executor-make)
                                        filename target)
  "Builds the make command that can execute the TARGET."

  (format "make -f %s -C %s %s"
          (shell-quote-argument filename)
          (file-name-directory filename)
          target))

(cl-defmethod executor-get-files ((exc executor-make))
  "Get all the makefiles in the current project"
  (-filter (lambda (f) (executor-handle-file-p exc f))
           (projectile-current-project-files)))

(cl-defmethod executor-get-files-initial-input ((exc executor-make) files)
  "From a list of files, return the Makefile closest to the current
  buffer.

If none can be found, returns empty string."
  ;; Get the dominating file dir so we can use that as initial input
  ;; This means that if we are in a large project with a lot
  ;; of Makefiles, the closest one will be the initial suggestion.
  (let* ((bn (or (buffer-file-name) default-directory))
         (fn (or (locate-dominating-file bn "Makefile")
                 (locate-dominating-file bn "makefile")))
         (relpath (file-relative-name fn (projectile-project-root))))
    ;; If we are at the root, we don't need the initial
    ;; input. If we have it as `./`, the Makefile at
    ;; the root will not be selectable, which is confusing. Hence, we
    ;; remove that
    (if (not (s-equals? relpath "./"))
        relpath
      "")))

(cl-defmethod executor-handle-file-p ((exc executor-make) filename)
  "Returns `t' if the executor in question can parse and execute FILENAME."
  (and (s-contains? "makefile" (s-downcase filename))
       (not (string-match executor-ignore filename))))

(provide 'executor-make)
