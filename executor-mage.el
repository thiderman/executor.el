;;* Magefiles
(defclass executor-mage (executor)
  ())

(cl-defmethod executor-get-targets ((exc executor-mage)
                                    filename)
  "Return a list of all the targets of a Magefile.

Receives FILENAME, and runs `mage' in the directory of said
file."

  (let* ((default-directory (f-dirname filename))
         (cache (executor-get-cache exc filename)))

    (if cache
        cache
      (executor-store-cache
       exc
       filename
       (rest (s-split "\n" (shell-command-to-string "mage -l") t))))))

(cl-defmethod executor-get-files ((exc executor-mage))
  "Get all the magefiles in the current project"
  (-filter (lambda (f) (executor-handle-file-p exc f))
           (projectile-current-project-files)))

(cl-defmethod executor-handle-file-p ((exc executor-mage) filename)
  "Returns `t' if the executor in question can parse and execute FILENAME."
  ;; TODO(thiderman): Should probably inspect the file and look for the mage
  ;; build tag rather than assume the name.
  (and (s-contains? "mage.go" (s-downcase filename))
       (not (string-match executor-ignore filename))))

(cl-defmethod executor-compile-command ((exc executor-mage)
                                        filename target)
  "Builds the mage command that can execute the TARGET."

  (format "mage -v %s" target))

(cl-defmethod executor-decorate-target ((exc executor-mage)
                                        target)
  "Splits the line and colorizes the namespace and the target"

  (let* ((words (s-split ":" (s-trim-left target)))
         (namespace (first words))
         (split (s-split " " (second words)))
         (target (first split))
         ;; This is a tad messy, but it's the simplest way to get the comment
         ;; strings to be their own separate part.
         (docs (s-join " " (rest split))))
    (concat
     (propertize namespace 'face 'font-lock-keyword-face)
     (propertize ":" 'face 'font-lock-comment-delimiter-face)
     (propertize target 'face 'font-lock-function-name-face)
     (propertize docs 'face 'font-lock-comment-delimiter-face))))

(cl-defmethod executor-clean-target ((exc executor-mage)
                                     target)
  "Remove the docstring and the fontification from the target."
  (substring-no-properties (first (s-split " " target))))

(cl-defmethod executor-after-save-action ((exc executor-mage) filename)
  "Clear the cache so that any target modifications show up."
  (executor-clear-cache exc filename))

(provide 'executor-mage)
